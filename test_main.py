from main import SimpleCalculator
import unittest

class SimpleCalculatorTest(unittest.TestCase):
    def test_add_numbers(self):
        calculator = SimpleCalculator()
        self.assertEqual(calculator.add(4, 5), 9)

    def test_add_multiple_numbers(self):
        numbers = range(100)
        calculator = SimpleCalculator()
        self.assertEqual(calculator.add(*numbers), 4950)

    def test_subtract_numbers(self):
        calculator = SimpleCalculator()
        self.assertEqual(calculator.sub(10, 3), 7)

    def test_mul_numbers(self):
        calculator = SimpleCalculator()
        self.assertEqual(calculator.mul(6, 4), 24)

    def test_mul_multiple_numbers(self):
        numbers = range(1, 10)
        calculator = SimpleCalculator()
        self.assertEqual(calculator.mul(*numbers), 362880)

    def test_div_numbers(self):
        calculator = SimpleCalculator()
        self.assertEqual(calculator.div(13, 2), 6.5)


    def test_div_by_zero_returns_inf(self):
         calculator = SimpleCalculator()
         self.assertEqual (calculator.div(5, 0), float('inf'))

if _name_ == 'main':
    unittest.main()